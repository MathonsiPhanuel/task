import React from "react";
import { Link, useLocation} from "react-router-dom";

const LeftNavBar=(props)=>{

    const location=useLocation()
    const path= location.pathname 
    return(
    <React.Fragment>
      <div className="sidebar" data-color="purple" data-background-color="black" data-image="../assets/img/sidebar-2.jpg">
      {/* <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    --> */}
      <div className="logo"><a href="http://www.creative-tim.com" className="simple-text logo-normal">
          Creative Tim
        </a></div>
      <div className="sidebar-wrapper">
        <ul className="nav">
          <li className={`nav-item ${path==='/' ? 'active' : '' }`}>
            <Link className="nav-link" to={'/'}>
              <i className="material-icons">dashboard</i>
              <p>Dashboard</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/profile' ? 'active' : '' }`}>
            <Link className="nav-link" to={'/profile'}>
              <i className="material-icons">person</i>
              <p>User Profile</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/tableList' ? 'active' : '' }`}>
            <Link className="nav-link" to={'/tableList'}>
              <i className="material-icons">content_paste</i>
              <p>Table List</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/typography' ? 'active' : '' }`}>
            <Link className="nav-link" to={'/typography'}>
              <i className="material-icons">library_books</i>
              <p>Typography</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/icons' ? 'active' : '' }`}>
            <Link className="nav-link" to={'/icons'}>
              <i className="material-icons">bubble_chart</i>
              <p>Icons</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/00' ? 'active' : '' }`}>
            <Link className="nav-link" href={'/maps'}>
              <i className="material-icons">location_ons</i>
              <p>Maps</p>
            </Link>
          </li>
          <li className={`nav-item ${path==='/notifications' ? 'active' : '' }`}>
            <a className="nav-link" href="/notifications">
              <i className="material-icons">notifications</i>
              <p>Notifications</p>
            </a>
          </li>
          {/* <!-- <li className="nav-item active-pro ">
                <a className="nav-link" href="./upgrade.html">
                    <i className="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> --> */}
        </ul>
      </div>
    </div>
    </React.Fragment>

    )
}

export default LeftNavBar