import React from "react";
import {Switch,BrowserRouter as Router, Route, redirect, useHistory} from "react-router-dom"
import Footer from "./components/Footer";
import LeftNavBar from "./components/LeftNavBar";
import Dashboard from "./components/pages/Dashboard";
import TableList from "./components/pages/TableList";
import UserProfile from "./components/pages/UserProfile";
import Typography from "./components/pages/Typography";
import Icons from "./components/pages/Icons";
import Notifications from "./components/pages/Notifications";
import TopNavBar from "./components/TopNavBar";

function App() {
  return (
<React.Fragment>

<div className="wrapper ">
              <Router>
 <LeftNavBar />
        <div className="main-panel">
           
<TopNavBar />
    
            <div className="content">
                <Switch>
                  <Route path="/" exact> 
                  <Dashboard/>
                  </Route>
                  <Route path="/profile" exact> 
                  <UserProfile/>
                  </Route>
                  <Route path="/tableList" exact> 
                  <TableList />
                  </Route>
                  <Route path="/typography" exact> 
                  <Typography />
                  </Route>
                  <Route path="/icons" exact> 
                  <Icons />
                  </Route>
                  <Route path="/notifications" exact> 
                  <Notifications />
                  </Route>
                  {/* <Route path="/" component={Dashboard} />
                  <Route path="/" component={UserProfile} />
                  <Route path="/tableList" component={TableList} /> */}
                </Switch>
             
             
            </div>
      <Footer />
       </div>
              </Router>
    </div>
</React.Fragment>
  );
}

export default App;
